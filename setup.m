global mfcm_setup

if mfcm_setup==1, return, end
%% Setup solvers

% MOSEK
pathmosek = 'path/to/mosekfolder'; % change
addpath([pathmosek '/mosek/8/toolbox/r2014aom/']);
% system(['export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:' pathmosek '/mosek/8/tools/platform/linux64x86/bin/libmosek64.so.8.0']);
% javaaddpath([pathmosek '/mosek/8/tools/platform/linux64x86/bin/mosekmatlab.jar']); 

% YALMIP
pathyalmip = 'path/to/yalmipfolder'; % change
addpath(genpath([pathyalmip '/yalmip']));


%% Geodesic lib
pathgeo = path/to/fast/marching/folder;
addpath(genpath([pathgeo '/Toolbox-Fast-Marching-Unix-master/']))
global geodesic_library;
geodesic_library = 'geodesic_matlab_api';      %"release" is faster and "debug" does additional checks

%% Set path
addpath(genpath('.'));

%% Plotting
set(groot, 'defaultTextInterpreter','latex');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');

mfcm_setup=1;