# Model-free Consensus Maximization for Non-Rigid Shapes

A basic implementation of the isometric shape matching algorithm as presented in the paper:

[Model-free Consensus Maximization for Non-Rigid Shapes](https://arxiv.org/abs/1807.01963)  
Thomas Probst, Ajad Chhatkuli, Danda Pani Paudel, Luc Van Gool  
European Conference on Computer Vision (ECCV) 2018  

## Requirements

Install the following tools, and set the paths in setup.m:

- [MOSEK 8](https://www.mosek.com/downloads/)
- [Fast Marching Toolbox](https://ch.mathworks.com/matlabcentral/fileexchange/6110-toolbox-fast-marching)
- [YALMIP](https://yalmip.github.io/download/)
- [GeodesicToolbox](https://ch.mathworks.com/matlabcentral/fileexchange/18168-exact-geodesic-for-triangular-meshes)

## MPI Faust
Download [MPI-FAUST](http://faust.is.tue.mpg.de/) and set the path in the faust_geodesic_synthetict.m script.

## Run
*faust_geodesic_synthetic.m* performs intra-subject matching for 10 subjects of the FAUST training set.  
Synthetic outlier matches are introduced, and recovered by the method.

