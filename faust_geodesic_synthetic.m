setup;
rng(42);

%% Experiment parameters
faust_path='faust/';

%Number of matches to sample (run on  multiple disjoint clusters if nMatches>N)
N=100;
%Set number of outliers (currently 80% of outliers)
nOutliers=round(0.8*N);

%%
it=0;
outlier_tp=[];
inlier_tp=[];
solveTime=[];

%Match 3 subjects in 2 different poses
for k_src=0:30:60
    
for k_tgt=(k_src+10):10:(k_src+20)
[k_src  k_tgt]   
it=it+1

%Load source mesh
mesh_src_path=sprintf('%s/training/registrations/tr_reg_%03d.ply',faust_path,k_src);
[d,f] = read_plyFull(mesh_src_path);
mshSrc.vertexPos=[d.x d.y d.z];
mshSrc.faces=f;
geomshSrc = geodesic_new_mesh(mshSrc.vertexPos, mshSrc.faces);
algDefSrc = geodesic_new_algorithm(geomshSrc, 'dijkstra');
nVertices=size(mshSrc.vertexPos,1);

%Load target mesh
mesh_src_path=sprintf('%s/training/registrations/tr_reg_%03d.ply',faust_path,k_tgt);
[d,f] = read_plyFull(mesh_src_path);
mshTgt.vertexPos=[d.x d.y d.z];
mshTgt.faces=f;
geomshTgt = geodesic_new_mesh(mshTgt.vertexPos, mshTgt.faces);
algDefTgt = geodesic_new_algorithm(geomshTgt, 'dijkstra');


matchesRefined=[];
correctPtsAll=[];
acceptedOutliersAll=[];
rejectedInliersAll=[];

%% Synthetic outliers
%Establish matches (gt registration)
sampleInd=randperm(nVertices,N);
matches=[sampleInd;sampleInd];
outlierIdx=randperm(N,nOutliers);

%ensure outliers are outliers 
%(note that outlier matches could still be close in geodesics)
while(1)
matches(2,outlierIdx)=randperm(nVertices,nOutliers);
undesiredInliers=sum(matches(1,outlierIdx)==matches(2,outlierIdx));
if undesiredInliers==0,break,end
end

outlier_gt = false(N,1);
outlier_gt(outlierIdx) = true;

%% Compute NBhoods for given point samples
templateSourceSparse=mshSrc.vertexPos(sampleInd,:)';
visibt = true(N,1);
Kneighbors = round(N);
[IDXn, ~] = getNeigDistanceTemplate(templateSourceSparse,Kneighbors,visibt);

%% Construct problem
%threshold on geodesics
eps1 = 0.25;

%define variables for ILP
z = binvar(N,1);
objective_final=sum(z);
cons = [];

%Go through all matches
for i = 1: size(matches,2)    
    srcPtIdx = matches(1,i);
    tgtPtIdx = matches(2,i);
    
    %Get all points to compare geodesic distances against
    srcNbIndsSparse = IDXn(i,IDXn(i,:)~=0);  
    srcNbInds = matches(1,srcNbIndsSparse);
    tgtNbInds = matches(2,srcNbIndsSparse);

    if ~isempty(srcNbInds)
        
        %compute geodesics on target mesh
        gt = computeAllGeodesic(algDefSrc,mshSrc,srcPtIdx,srcNbInds);
        %compute geodesics on source mesh
        gd = computeAllGeodesic(algDefTgt,mshTgt,tgtPtIdx,tgtNbInds);
        
        %Compute rel. distances
        curdist = 0.5*(abs(gt-gd)./gt + abs(gt-gd)./gd);
  
            
        for l = 1: length(curdist)
            if curdist(l)>eps1
                cons = [cons, (z(i) + z(srcNbIndsSparse(l))>=1)];

            end
        end 
                
    end
end


%% Mosek solver options
disp('mosek');
tic;

options = sdpsettings('solver','bnb', 'verbose', 0);
options.bnb.maxiter = 100000;
options.bnb.method = 'best';
options.bnb.gaptol = 1e-5;
options.bnb.inttol = 1e-5;
options.bnb.prunetol = 1e-05; % default: 1.0000e-04
options.bnb.solver = 'mosek';

acc = 1e-09;
options.mosek.MSK_DPAR_INTPNT_CO_TOL_PFEAS = acc;
options.mosek.MSK_DPAR_INTPNT_CO_TOL_DFEAS = acc;
options.mosek.MSK_DPAR_INTPNT_CO_TOL_REL_GAP = acc;

%% Solve ILP
optimize(cons,objective_final,options);
% ts{oIdx} = toc;
timing=toc

%retrieve solution of z. z=1 for outliers, z=0 for inliers
z_sol = double(z);
zn = z_sol>0.1;

%Evaluate
nOutliers
difference = nnz(zn - outlier_gt)
num_detected_true = numel(intersect(find(zn),outlierIdx))
num_Inliers_true = numel(intersect(find(~zn),setdiff([1:N],outlierIdx)))

accepted_outliers=intersect(find(~zn),outlierIdx);
rejected_inliers=intersect(find(zn>0),setdiff([1:N],outlierIdx));
correct_pts=setdiff(setdiff(1:N,outlierIdx),rejected_inliers);

matchesRefined=[matchesRefined matches(:,find(~zn))];
correctPtsAll=[correctPtsAll matches(:,correct_pts)];
acceptedOutliersAll=[acceptedOutliersAll matches(:,accepted_outliers)];
rejectedInliersAll=[rejectedInliersAll matches(:,rejected_inliers)];

outlier_tp(it)=num_detected_true;
inlier_tp(it)=num_Inliers_true;
solveTime(it)=timing;


end
end

%% Plots
figure(1)
bar([outlier_tp; 80-outlier_tp;inlier_tp ]','stacked');
