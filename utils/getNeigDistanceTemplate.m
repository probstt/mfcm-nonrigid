function [ IDX, D ] = getNeigDistanceTemplate( p, Ng, visb )
% get triangulation using pdist functions: Ng number of neighbors


N = length(p);

distmat = pdist2(p',p','euclidean');    
distmat(~visb,:) = -1;    


[D, IDX] = sort(distmat,2);

IDX = IDX(:,1:Ng);
D = D(:,2:Ng);



                
end